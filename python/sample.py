# -*- coding: utf-8 -*-

# Copyright :copyright: 2019 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email: ibprnd@gmail.com

import os

from dropshipping.utils.tracking_services import EsTrackingService

host = os.getenv('ES_HOST', 'localhost')
port = os.getenv('ES_PORT', '9200')
user = os.getenv('ES_USER', '')
password = os.getenv('ES_PASSWORD', '')
tracking_service = EsTrackingService(host, port, user, password)

# Get alll orders without tracking
for order_id in tracking_service.get_untracked_orders():
    print(order_id)
# Get all orders count on service
print(tracking_service.get_orders_count())
# Get orders count without tracking on service
print(tracking_service.get_untracked_orders_count())
# Get orders count with tracking on service
print(tracking_service.get_tracked_orders_count())
# Get tracking for orders (a list of order ids)
print(tracking_service.get_trackings('113-8323758-0185002'))

# List all carriers
carriers = dict()
for tracking_info in tracking_service.list_trackings():
    tracking = tracking_info.get('tracking', None)
    if not tracking:
        continue

    carrier, tracking_number = tracking.split(':')
    carrier = carrier.strip().upper()
    carriers.setdefault(carrier, 0)
    carriers[carrier] += 1

for carrier in sorted(carriers, key=carriers.get, reverse=True):
    print('Carrier: %s, Trackings: %d' % (carrier, carriers[carrier]))

# Get tracking statuses
trackings = [
    'UPS:1Z82V5490126598560',
    'UPS Mail Innovations:92419901093299583003136686',
    'USPS:9361289670090529619626',
    'Amazon:TBA876828088000'
]
print(tracking_service.get_tracking_statuses(trackings))
