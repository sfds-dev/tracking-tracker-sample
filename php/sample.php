<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

use TrackingTracker\TrackingTracker;

require_once './vendor/autoload.php';

$host = getenv('ES_HOST') ?: 'localhost';
$port = getenv('ES_PORT') ?: 9200;
$user = getenv('ES_USER') ?: '';
$password = getenv('ES_PASSWORD') ?: '';
$tracker = new TrackingTracker($host, $port, $user, $password);

// # Get alll orders without tracking
foreach ($tracker->getUntrackedOrders() as $orderId) {
	print($orderId . "\n");
}

// Get all orders count on service
print($tracker->getOrdersCount());
print("\n");

// Get orders count without tracking on service
print($tracker->getUntrackedOrdersCount());
print("\n");

// Get orders count with tracking on service
print($tracker->getTrackedOrdersCount());
print("\n");

# List all carriers
$carriers = array();
foreach ($tracker->listTrackings() as $tracking) {
	if (!array_key_exists('tracking', $tracking) || !$tracking['tracking']) {
		continue;
	}
	list($carrier, $trackingNumber) = explode(':', $tracking['tracking']);
	$carrier = strtoupper($carrier);
	if (array_key_exists($carrier, $carriers)) {
		$carriers[$carrier] += 1;
	} else {
		$carriers[$carrier] = 1;
	}
}
arsort($carriers);
foreach ($carriers as $carrier => $trackingCnt) {
	print('Carrier: ' . $carrier . ', Trackings: ' . $trackingCnt . "\n");
}

// Get tracking for orders (a list of order ids)
print_r($tracker->getTrackings(['113-8323758-0185002']));

# Get tracking statuses
$trackings = [
    'UPS:1Z82V5490126598560',
    'UPS Mail Innovations:92419901093299583003136686',
    'USPS:9361289670090529619626',
    'Amazon:TBA876828088000'
];
print_r($tracker->getTrackingStatuses($trackings));

$result = $tracker->saveTrackings(array(
	'1970000202' => 'GM555346914000402815',
	'1920000308' => 'GM555346914000402761',
	'1860000135' => 'GM555346914000402846'
));
print_r($result);
